#!/bin/bash

MAPIVER=${MAPIVER:-latest}

set -e

docker build $BUILD_ARGS \
    --build-arg mapiver=${MAPIVER} \
    -f debian/builder.dock \
    -t xdc-builder \
    .

docker run -v `pwd`:/xdc xdc-builder /xdc/build-deb.sh
