package main

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	bctl "gitlab.com/mergetb/tech/beluga/pkg/belugactld"
	"google.golang.org/grpc"
)

var (
	server = ""
)

func powerCmds(root *cobra.Command) {

	power := &cobra.Command{
		Use:   "power <cmd>",
		Short: "Commands related to power for experiment nodes",
	}
	power.PersistentFlags().StringVarP(&server, "belugactl", "b", "belugactl:6941", "Power control daemon endpoint")

	var hard bool
	cycle := &cobra.Command{
		Use:   "cycle <node> <node> ... <node>",
		Short: "Power cycle the given node (node can be experiment or site-resource name)",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			exitIfUnattached()
			doCycle(args, hard)
		},
	}
	cycle.Flags().BoolVarP(&hard, "hard", "", true, "if given, hard power cycle")
	power.AddCommand(cycle)

	off := &cobra.Command{
		Use:   "off <node> <node> ... <node>",
		Short: "Power off the given node (node can be experiment or site-resource name)",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			exitIfUnattached()
			doPower(args, true)
		},
	}
	power.AddCommand(off)

	on := &cobra.Command{
		Use:   "on <node> <node> ... <node>",
		Short: "Power on the given node (node can be experiment or site-resource name)",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			exitIfUnattached()
			doPower(args, false)
		},
	}
	power.AddCommand(on)

	status := &cobra.Command{
		Use:   "status <node> <node> ... <node>",
		Short: "Show the status if the given node (node can be experiment or site-resource name)",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			exitIfUnattached()
			doStatus(args)
		},
	}
	power.AddCommand(status)

	root.AddCommand(power)
}

func doCycle(nodes []string, soft bool) {

	var err error
	withBelugacltdClient(func(c bctl.BelugactldClient) {
		var e error
		if soft {
			_, e = c.SoftCycle(
				context.TODO(),
				&bctl.SoftCycleRequest{
					Nodeids: nodes,
				},
			)
		} else {
			_, e = c.HardCycle(
				context.TODO(),
				&bctl.HardCycleRequest{
					Nodeids: nodes,
				},
			)
		}
		err = e
	})

	if err != nil {
		log.Fatalf("Error: %s", err)
	}
}

func doPower(nodes []string, off bool) {

	var err error
	withBelugacltdClient(func(c bctl.BelugactldClient) {
		if off == true {
			_, err = c.PowerOff(
				context.TODO(),
				&bctl.PowerOffRequest{
					Nodeids: nodes,
				},
			)
		} else {
			_, err = c.PowerOn(
				context.TODO(),
				&bctl.PowerOnRequest{
					Nodeids: nodes,
				},
			)
		}
	})

	if err != nil {
		log.Fatalf("Error: %s", err)
	}
}

func doStatus(nodes []string) {

	var err error
	var reply *bctl.StatusReply
	withBelugacltdClient(func(c bctl.BelugactldClient) {
		reply, err = c.Status(
			context.TODO(),
			&bctl.StatusRequest{
				Nodeids: nodes,
			},
		)
	})

	if err != nil {
		log.Fatalf("Error: %s", err)
	}

	// Use COLORs to very nicely display the table of status results in COLOR.
	fmt.Fprintf(tw, "%s\t%s\t%s\n",
		neutral("Resource"), neutral("Experiment Name"), neutral("Status"),
	)
	for _, s := range reply.Status {
		fmt.Fprintf(tw, "%s\t%s\t%s\n",
			blue(s.Resource),
			green(s.Xpname),
			neutral(s.Status),
		)
	}

	tw.Flush()
}

func withBelugacltdClient(f func(c bctl.BelugactldClient)) {

	conn, err := grpc.Dial(fmt.Sprintf("%s", server), grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	c := bctl.NewBelugactldClient(conn)

	f(c)

}
