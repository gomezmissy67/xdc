package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

func tunnelCmds(root, show *cobra.Command) {

	tunnel := &cobra.Command{
		Use:   "tunnel",
		Short: "Show the xdc tunnel status",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			showTunnel()
		},
	}
	show.AddCommand(tunnel)

	attachCmd := &cobra.Command{
		Use:   "attach <pid> <eid> <rid>",
		Short: "Build a tunnel between this XDC and the given materialization.",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			attach(args[0], args[1], args[2])
		},
	}
	root.AddCommand(attachCmd)

	detachCmd := &cobra.Command{
		Use:   "detach",
		Short: "Close any tunnel between this XDC and a materialization.",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			detach()
		},
	}
	root.AddCommand(detachCmd)
}

func attach(pid, eid, rid string) {

	if p, e, r, _ := readTunnelInfo(); p != "" {

		if p == pid && e == eid && r == rid {
			fmt.Printf("Already attached to %s %s %s\n", p, e, r)
			return
		}

		if p != pid || e != eid || r != rid {
			detach()
		}
	}

	fmt.Printf("attaching to %s %s %s\n", pid, eid, rid)

	host, err := hostname()
	if err != nil {
		doError(err)
	}

	err = mapi.Attach(pid, eid, rid, host)
	if err != nil {
		doError(err)
	}

	err = writeFile(pid, "pid")
	if err != nil {
		doError(err)
	}

	err = writeFile(eid, "eid")
	if err != nil {
		doError(err)
	}

	err = writeFile(rid, "rid")
	if err != nil {
		doError(err)
	}

	updateResolve(pid, eid, rid)
}

func detach() {

	pid, eid, rid, err := readTunnelInfo()
	if err != nil {
		doError(NotAttached)
	}

	restoreResolve()

	host, err := hostname()
	if err != nil {
		doError(err)
	}

	fmt.Printf("detaching from %s %s %s\n", pid, eid, rid)

	err = mapi.Detach(pid, eid, rid, host)
	if err != nil {
		doError(err)
	}

	deleteFile("pid")
	deleteFile("eid")
	deleteFile("rid")
}

func hostname() (string, error) {

	host, err := os.Hostname()
	if err != nil {
		doError(err)
	}

	names := strings.Split(host, ".")

	if len(names) == 0 {
		return "", fmt.Errorf("Unable to get hostname of the XDC")
	}

	return names[0], nil
}

func readTunnelInfo() (string, string, string, error) {

	pid, err := readFile("pid")
	if err != nil {
		return "", "", "", err
	}

	eid, err := readFile("eid")
	if err != nil {
		return "", "", "", err
	}

	rid, err := readFile("rid")
	if err != nil {
		return "", "", "", err
	}

	return pid, eid, rid, nil
}

func showTunnel() {

	// Make sure there's one and only one wireguard interface.
	name, addr, up, err := getWGInfo()
	if err != nil {
		fmt.Printf("xdc not attached: %s\n", err)
		os.Exit(1)
	}

	pid, eid, rid, err := readTunnelInfo()
	if err != nil {
		fmt.Println("xdc may not be attached: no materialization information.")
		os.Exit(1)
	}

	flag := "UP"
	if !up {
		flag = "DOWN"
	}

	fmt.Printf("Materialization: %s %s %s\n", pid, eid, rid)
	fmt.Printf("Interface: %s (%s)\n", name, flag)
	fmt.Printf("Address: %s\n", addr)
}

func exitIfUnattached() {
	if !isAttached() {
		log.Fatalf("Command failed: you must be attached to a materialization to execute this.")
	}
}

func isAttached() bool {
	_, _, _, err := getWGInfo()
	return err == nil
}

func getWGInfo() (string, string, bool, error) {

	// Note: cannot use rtnl here on xdcs.
	ifaces, err := net.Interfaces()
	if err != nil {
		return "", "", false, err
	}

	_, wgnet, _ := net.ParseCIDR("192.168.254.0/24")

	for _, iface := range ifaces {
		// We "cheat" here by using the well-known xdc wg network 192.168.254.0/24
		addrs, err := iface.Addrs()
		if err != nil {
			return "", "", false, err
		}

		if len(addrs) == 0 {
			return "", "", false, fmt.Errorf("no addrs in tunnel network found")
		}

		_, ipnet, err := net.ParseCIDR(addrs[0].String())
		if err != nil {
			return "", "", false, err
		}

		if wgnet.Contains(ipnet.IP) {
			up := iface.Flags&net.FlagUp == 1
			return iface.Name, addrs[0].String(), up, nil
		}
	}

	return "", "", false, fmt.Errorf("no wireguard interface found")
}

func updateResolve(pid, eid, rid string) {

	// We want to put our rid.eid.pid name at the start of "search"
	// and our DNS addr as the first nameserver. LIke this:
	// search demo.rohus.losangeles xdc.svc.cluster.local svc.cluster.local cluster.local
	// nameserver 172.30.0.1
	// nameserver 10.77.0.2

	// There is probably a smarter way to do this. And it will probably break once the XDC
	// base OS changes.

	// Save existing.
	orig, err := ioutil.ReadFile("/etc/resolv.conf")
	if err != nil {
		doError(err)
	}
	err = writeFile(string(orig), "resolv")
	if err != nil {
		doError(err)
	}

	out := []string{}

	buf, err := os.Open("/etc/resolv.conf")
	if err != nil {
		doError(err)
	}

	scnr := bufio.NewScanner(buf)
	for scnr.Scan() {
		line := scnr.Text()

		if strings.HasPrefix(line, "search") {
			tkns := strings.Split(line, " ")
			name := fmt.Sprintf("%s.%s.%s", rid, eid, pid)

			// if not there, add it.
			if !strings.Contains(line, name) {
				l := fmt.Sprintf("search %s %s", name, strings.Join(tkns[1:], " "))
				out = append(out, l)
				out = append(out, "nameserver 172.30.0.1")
			}
			continue
		}

		out = append(out, line)
	}

	err = scnr.Err()
	if err != nil {
		buf.Close()
		doError(err)
	}

	buf.Close()

	data := strings.Join(out, "\n")
	err = ioutil.WriteFile("/etc/resolv.conf", []byte(data), 0666)
	if err != nil {
		doError(err)
	}
}

func restoreResolve() {

	data, err := readFile("resolv")
	if err != nil {
		doError(err)
	}

	err = ioutil.WriteFile("/etc/resolv.conf", []byte(data), 0666)
	if err != nil {
		doError(err)
	}

	deleteFile("resolv")
}
