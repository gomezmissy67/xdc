module gitlab.com/mergetb/portal/xdc

go 1.13

require (
	github.com/mattn/go-isatty v0.0.12
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/cobra v1.0.0
	gitlab.com/mergetb/engine v0.5.13
	gitlab.com/mergetb/mergeapi v0.1.17
	gitlab.com/mergetb/tech/beluga v0.1.12
	gitlab.com/mergetb/xir v0.2.15
	golang.org/x/crypto v0.0.0-20200604202706-70a84ac30bf9
	google.golang.org/grpc v1.27.0
)
