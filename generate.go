package main

import (
	"encoding/json"
	"fmt"
	"net"
	"sort"
	"strings"

	"github.com/spf13/cobra"
	xir "gitlab.com/mergetb/xir/lang/go/v0.2"
)

func generateCmds(root *cobra.Command) {

	generate := &cobra.Command{
		Use:   "generate [thing]",
		Short: "Generate files or configurations",
	}

	var prefix string
	etchosts := &cobra.Command{
		Use:   "etchosts [pid eid rid]",
		Short: "Generate an /etc/hosts file based on a realization. If no args, generate based on attached materialization.",
		Args:  cobra.MaximumNArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			exitIfUnattached()
			if len(args) != 3 {
				etcHosts("", "", "", prefix)
			} else {
				etcHosts(args[0], args[1], args[2], prefix)
			}
		},
	}
	etchosts.Flags().StringVarP(&prefix, "prefix", "p", "", "If given prefix this to the node names.")

	generate.AddCommand(etchosts)

	root.AddCommand(generate)
}

func etcHosts(pid, eid, rid, prefix string) {

	model := getModel(pid, eid, rid)
	dumpEtcHosts(model, prefix)
}

func dumpEtcHosts(xNet *xir.Net, prefix string) {

	type addrProps struct {
		Addrs []string `json:"addrs"`
	}

	data := map[string][]string{}
	seen := map[string]bool{}

	for _, node := range xNet.AllNodes() {
		for epi, ep := range node.Endpoints {
			ipdata, ok := ep.Props["ip"]
			if ok {
				ips := &addrProps{}
				bytes, _ := json.Marshal(ipdata)
				json.Unmarshal(bytes, ips)

				for _, addr := range ips.Addrs {
					ip, _, _ := net.ParseCIDR(addr)
					a := ip.String()

					// If this is the first time we've seen this node, make this the default
					// entry for the node name.
					if _, ok := seen[node.Id]; !ok {
						data[a] = append(data[a], prefix+node.Id)
						seen[node.Id] = true
					} else {
						data[a] = append(data[a], fmt.Sprintf("%s%s-%d", prefix, node.Id, epi))
					}
				}
			}
		}
	}

	keys := []string{}
	for addr := range data {
		keys = append(keys, addr)
	}
	sort.Strings(keys)

	for _, key := range keys {
		fmt.Printf("%s %s\n", key, strings.Join(data[key], "\t"))
	}
}
